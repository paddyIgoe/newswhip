# What does this app do?

This app gets the latest Twitter trends and related tweets for a country.

## Running the app

Gradle is the app's build tool. Please run the "bootRun" Gradle task to start the app
(via an embedded Tomcat server) on port 8080.

You must supply a "api.twitter.oauth2.consumerSecret" JVM argument when running this
task. This argument should be your Twitter consumer secret key and can be found in your developer account app dashboard.
Providing Gradle is installed on your machine and your terminal is in the root of this project,
you can run this command to start the app 

```
gradle bootRun -Dpi.twitter.oauth2.consumerSecret={insert secret key here}
```

Along with the Twitter secret key you must also supply the Twitter consumer key as the "api.twitter.oauth2.consumerKey"
value in the "src/main/resources/application.properties" file.

## How do I use this app?

After starting the application server you can send requests to the URL
"http://{server}:{port}/twitter/trends/country/{ISO 3166-1 alpha-2 code}". The application will then retrieve
Twitter trends and tweets for the country code you specified.

## Features

* Requests are cached by country code for 5 minutes by default. This can be configured via the application.properties file
mentioned above (request.cache.duration,request.cache.unit). Unit must be parsable from a java.time.temporal.ChronoUnit entry.
* Twitter uses Yahoo's WOEId to identify a location when searching for trends. WOEIds are cached by the application.
* Trends and tweets are send back to the requester along with being written to a .csv file. The location of this file is
configurable in the application.properties file (output.csv.path).
* Supported country codes can be configured in the application.properties file (validation.countryCodes).

## Shortcomings

* Automated testing has not been implemented. This was mainly due to time constraints.
* Documentation and code commenting could be improved substantially.

## Improvements

* Implementation using reactive non-blocking IO.
* Creating a containerisation file, e.g. Docker, so app can be easily deployed across environments.
* Error codes and user messages are all in English a message code resolution solution would be useful.
* Some other TODO elements that are highlighted in the code.

