package com.newswhip.test;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.newswhip.test")
@Slf4j
public class Main {

    public static void main(final String[] args) {
        log.info("Starting NewsWhip coding challenge app");
        SpringApplication.run(Main.class, args);
    }

}
