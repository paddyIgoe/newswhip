package com.newswhip.test.config;

import com.newswhip.test.model.inbound.RequestParameter;
import com.newswhip.test.service.RequestHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

import javax.inject.Inject;

@Configuration
@Slf4j
public class RoutingConfig {

    private final RequestHandler requestHandler;

    @Inject
    public RoutingConfig(final RequestHandler requestHandler) {
        this.requestHandler = requestHandler;
    }

    @Bean
    public RouterFunction<ServerResponse> routerFunction() {
        return twitterRequestRouter();
    }

    protected RouterFunction<ServerResponse> twitterRequestRouter() {
        return RouterFunctions.route(RequestPredicates.GET("/twitter/trends/country/{countryCode}"),
                request -> {
                    RequestParameter requestParameter = new RequestParameter("countryCode", request.pathVariable("countryCode"));
                    log.debug("Request for trends and their most popular tweets in '{}'", requestParameter.getValue());
                    return requestHandler.findTrendsAndTop10TweetsByCountry(requestParameter);
                });
    }

}
