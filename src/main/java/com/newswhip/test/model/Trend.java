package com.newswhip.test.model;

import com.newswhip.test.model.inbound.twitter.trend.InboundTrend;
import lombok.Data;

import java.net.URL;

@Data
public class Trend {

    private final String name;
    private final URL url;
    private final int tweetVolume;

    public Trend(final InboundTrend trendResponse) {
        this.name = trendResponse.getName();
        this.url = trendResponse.getUrl();
        this.tweetVolume = trendResponse.getTweetVolume();
    }

}
