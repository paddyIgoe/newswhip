package com.newswhip.test.model;

import com.newswhip.test.model.inbound.twitter.tweet.Hashtag;
import com.newswhip.test.model.inbound.twitter.tweet.InboundTweet;
import lombok.Data;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class Tweet {

    private final String id;
    private final ZonedDateTime createdAt;
    private final String content;
    private final List<String> hashtags;

    public Tweet(final InboundTweet inboundTweet) {
        this.id = inboundTweet.getId();
        this.createdAt = ZonedDateTime.ofInstant(inboundTweet.getCreatedAt().toInstant(), ZoneId.systemDefault());
        this.content = inboundTweet.getContent();
        this.hashtags = inboundTweet.getEntities().getHashtags().stream().map(Hashtag::getText).collect(Collectors.toList());
    }

}
