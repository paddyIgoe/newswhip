package com.newswhip.test.model;

import com.newswhip.test.model.outbound.TweetsByTrend;
import lombok.Data;

import java.time.Instant;
import java.util.List;

@Data
public class TweetsByTrendCache {

    private final Instant created;
    private final List<TweetsByTrend> tweetsByTrends;

}
