package com.newswhip.test.model.inbound;

import lombok.Data;

@Data
public class RequestParameter {

    private final String name;
    private final String value;

}
