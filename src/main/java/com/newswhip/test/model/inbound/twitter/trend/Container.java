package com.newswhip.test.model.inbound.twitter.trend;

import lombok.Data;

import java.util.List;

@Data
public class Container {

    private List<InboundTrend> trends;

}
