package com.newswhip.test.model.inbound.twitter.trend;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.net.URL;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class InboundTrend {

    private String name;

    private URL url;

    @JsonProperty("tweet_volume")
    private int tweetVolume;

}
