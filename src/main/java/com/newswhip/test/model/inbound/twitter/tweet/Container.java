package com.newswhip.test.model.inbound.twitter.tweet;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Collection;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Container {

    @JsonProperty("statuses")
    private Collection<InboundTweet> inboundTweets;

}
