package com.newswhip.test.model.inbound.twitter.tweet;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.Collection;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Entity {

    private Collection<Hashtag> hashtags;

}
