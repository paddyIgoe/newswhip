package com.newswhip.test.model.inbound.twitter.tweet;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Hashtag {

    private String text;

}
