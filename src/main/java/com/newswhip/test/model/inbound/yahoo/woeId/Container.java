package com.newswhip.test.model.inbound.yahoo.woeId;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Container {

    private Query query;

}
