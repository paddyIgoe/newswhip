package com.newswhip.test.model.inbound.yahoo.woeId;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Place {

    @JsonProperty("woeid")
    private String woeId;

}
