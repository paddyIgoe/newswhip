package com.newswhip.test.model.inbound.yahoo.woeId;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.Instant;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Query {

    private int count;

    @JsonProperty("created")
    private Instant createdAt;

    @JsonProperty("lang")
    private String language;

    @JsonProperty("results")
    private Result result;

}
