package com.newswhip.test.model.outbound;

import lombok.Data;

import java.util.Collection;

@Data
public class RequestErrorContainer {

    private final String message;
    private final Collection<RequestParameterError> parameterErrors;

}
