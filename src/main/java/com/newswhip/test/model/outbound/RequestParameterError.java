package com.newswhip.test.model.outbound;

import lombok.Data;

@Data
public class RequestParameterError {

    private final String parameter;
    private final String message;

}
