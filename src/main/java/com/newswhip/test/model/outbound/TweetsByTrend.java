package com.newswhip.test.model.outbound;

import com.newswhip.test.model.Tweet;
import lombok.Data;

import java.util.List;

@Data
public class TweetsByTrend {

    private final String trend;
    private final List<Tweet> tweets;

}
