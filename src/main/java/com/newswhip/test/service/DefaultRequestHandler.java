package com.newswhip.test.service;

import com.newswhip.test.model.Trend;
import com.newswhip.test.model.Tweet;
import com.newswhip.test.model.TweetsByTrendCache;
import com.newswhip.test.model.inbound.RequestParameter;
import com.newswhip.test.model.outbound.RequestErrorContainer;
import com.newswhip.test.model.outbound.RequestParameterError;
import com.newswhip.test.model.outbound.TweetsByTrend;
import com.newswhip.test.service.external.TwitterAPI;
import com.newswhip.test.util.CSVFileWriter;
import com.newswhip.test.validation.CountryCode2CharValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.validation.DirectFieldBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Service
@Slf4j
public class DefaultRequestHandler implements RequestHandler {

    private final TwitterAPI twitterAPI;
    private final CountryCode2CharValidator countryCodeValidator;
    private final CSVFileWriter csvFileWriter;

    private final String csvFileLocation;
    private final String genericExceptionResponse;

    private Map<String, TweetsByTrendCache> tweetsByTrendsCache = new ConcurrentHashMap<>();
    private long cacheDuration;
    private ChronoUnit cacheChronoUnit;

    @Inject
    public DefaultRequestHandler(final TwitterAPI twitterAPI, final CountryCode2CharValidator countryCodeValidator, final CSVFileWriter csvFileWriter,
                                 @Value("${output.csv.path?:/tmp/tweets_by_trends_.csv}") final String csvFileLocation,
                                 @Value("${exception.catchAll.message?:There was a problem processing your request; please inform the administrator to check the log files for the cause}") final String genericExceptionResponse,
                                 @Value("${request.cache.duration?:5}") final String cacheDuration,
                                 @Value("${request.cache.unit?:MINUTES}") final String cacheChronoUnit) {
        this.twitterAPI = twitterAPI;
        this.countryCodeValidator = countryCodeValidator;
        this.csvFileWriter = csvFileWriter;
        this.csvFileLocation = csvFileLocation;
        this.genericExceptionResponse = genericExceptionResponse;
        this.cacheDuration = Long.parseLong(cacheDuration);
        this.cacheChronoUnit = ChronoUnit.valueOf(cacheChronoUnit);
    }

    @Override
    public Mono<ServerResponse> findTrendsAndTop10TweetsByCountry(final RequestParameter countryCodeParameter) {
        try {
            Errors countryCodeValidation = validateCountryCodeParameter(countryCodeParameter, countryCodeValidator);
            if (countryCodeValidation.hasErrors()) {
                log.error("Country code validation error(s) found for '{}'", countryCodeParameter.getValue());
                return ServerResponse.badRequest()
                        .syncBody(new RequestErrorContainer("Your request contained invalid data; please review the errors", countryCodeValidation.getAllErrors().stream()
                                .map(errors -> new RequestParameterError(countryCodeParameter.getName(), errors.getDefaultMessage()))
                                .collect(Collectors.toList()))
                        );
            }
            List<TweetsByTrend> tweetsByTrends = getTweetsByTrend(countryCodeParameter, twitterAPI);
            writeTweetsByTrendsToCSVFileAsync(tweetsByTrends, csvFileWriter, countryCodeParameter);
            return ServerResponse.ok().syncBody(tweetsByTrends);
        }
        catch (Exception e) {
            log.error("Exception processing this request message: '{}'", e.getMessage());
            e.printStackTrace();
            return ServerResponse.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(Mono.just(genericExceptionResponse), String.class);
        }
    }

    protected Errors validateCountryCodeParameter(final RequestParameter countryCodeParameter, final CountryCode2CharValidator validator) {
        log.debug("Validating '{}' request parameter value '{}'", countryCodeParameter.getName(), countryCodeParameter.getValue());
        Errors countryCodeValidation = new DirectFieldBindingResult(countryCodeParameter.getValue(), countryCodeParameter.getName());
        ValidationUtils.invokeValidator(validator, countryCodeParameter.getValue(), countryCodeValidation);
        return countryCodeValidation;
    }

    protected List<TweetsByTrend> getTweetsByTrend(final RequestParameter countryCodeParameter, final TwitterAPI twitterAPI) {
        List<TweetsByTrend> tweetsByTrends = new ArrayList<>();
        if (tweetsByTrendsCache.containsKey(countryCodeParameter.getValue()) &&
                !(Duration.between(tweetsByTrendsCache.get(countryCodeParameter.getValue()).getCreated(),Instant.now())
                        .compareTo(Duration.of(cacheDuration, cacheChronoUnit)) > 0)) {
            log.info("Found cached tweets by trend for countryCode: '{}'", countryCodeParameter.getValue());
            tweetsByTrends = tweetsByTrendsCache.get(countryCodeParameter.getValue()).getTweetsByTrends();
        }
        else {
            log.trace("Retrieving tweets by trend via Twitter API");
            List<Trend> trends = twitterAPI.getTrendsByCountry(countryCodeParameter.getValue());
            for (Trend trend : trends) {
                tweetsByTrends.add(new TweetsByTrend(trend.getName(), twitterAPI.getTop10TweetsByTrend(trend)));
            }
            log.debug("Caching tweets by trend for countryCode: '{}'", countryCodeParameter.getValue());
            tweetsByTrendsCache.put(countryCodeParameter.getValue(), new TweetsByTrendCache(Instant.now(), tweetsByTrends));
        }
        return tweetsByTrends;
    }

    protected void writeTweetsByTrendsToCSVFileAsync(final List<TweetsByTrend> tweetsByTrends, final CSVFileWriter csvFileWriter, final RequestParameter countryCodeParameter) {
        log.trace("Asynchronously writing tweets by trend to .csv file");
        new Thread(() -> {
            try {
                int lastDot = csvFileLocation.lastIndexOf('.');
                String csvFile  = csvFileLocation.substring(0,lastDot) +
                        countryCodeParameter.getValue() +
                        "_" +
                        ZonedDateTime.now(ZoneOffset.UTC).format(DateTimeFormatter.ISO_INSTANT) +
                        csvFileLocation.substring(lastDot);
                List<List<String>> csvTweetsByTrends = new ArrayList<>();
                for (TweetsByTrend tweetsByTrend : tweetsByTrends) {
                    for (int j = 0; j < tweetsByTrend.getTweets().size(); j++) {
                        Tweet tweet = tweetsByTrend.getTweets().get(j);
                        csvTweetsByTrends.add(Arrays.asList(tweetsByTrend.getTrend(), tweet.getId(), tweet.getCreatedAt().toString(), tweet.getContent(), tweet.getHashtags().toString()));
                    }
                }
                List<String> headers = Arrays.asList("trend", "id", "created_at", "content", "hashtags");
                log.info("Writing tweets by trends to '{}'", csvFile);
                csvFileWriter.write(new File(csvFile), headers, csvTweetsByTrends);
            }
            catch (IOException e) {
                log.error("Unable to write CSV file for country code: '{}'; message: '{}'", countryCodeParameter.getValue(), e.getMessage());
                e.printStackTrace();
            }
        }).start();

    }

}
