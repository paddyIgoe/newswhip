package com.newswhip.test.service;

import com.newswhip.test.model.inbound.RequestParameter;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

public interface RequestHandler {

    Mono<ServerResponse> findTrendsAndTop10TweetsByCountry(RequestParameter countryCodeParameter);

}
