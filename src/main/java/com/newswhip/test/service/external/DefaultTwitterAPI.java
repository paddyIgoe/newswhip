package com.newswhip.test.service.external;

import com.google.common.collect.ImmutableMap;
import com.newswhip.test.model.Trend;
import com.newswhip.test.model.Tweet;
import com.newswhip.test.model.inbound.twitter.trend.InboundTrend;
import com.newswhip.test.model.inbound.twitter.tweet.Container;
import com.newswhip.test.model.inbound.twitter.oauth2.Response;
import com.newswhip.test.util.URIHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import javax.inject.Inject;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED;

// TODO: Failure of retrieval methods in this class may be because OAuth2 token was invalidated. It'd be nice to figure this out and refresh the token. Currently a server restart will fix this issue

@Component
@Slf4j
public class DefaultTwitterAPI implements TwitterAPI {

    private final YahooAPI yahooAPI;
    private final URIHelper uriHelper;

    private final URI oauth2URI;
    private final String oauth2ConsumerKey;
    private final String oauth2ConsumerSecret;
    private final Charset oauth2Charset;
    private final String oauth2RequestBody;

    private String oauth2Token;

    private final URI trendsByPlaceBaseURI;
    private final Map<String, String> trendsByPlaceParameters;

    private final URI tweetsByTrendURI;
    private final Map<String, String> tweetsByTrendParameters;

    // TODO: think about moving these values to their own config class
    @Inject
    public DefaultTwitterAPI(final YahooAPI yahooAPI, final URIHelper uriHelper,
                             @Value("${api.twitter.oauth2.uri?:https://api.twitter.com/oauth2/token}") final String oauth2URI,
                             @Value("${api.twitter.oauth2.consumerKey}") final String oauth2ConsumerKey,
                             @Value("${api.twitter.oauth2.consumerSecret}") final String oauth2ConsumerSecret,
                             @Value("${api.twitter.oauth2.charset?:UTF-8}") final String oauth2Charset,
                             @Value("${api.twitter.oauth2.request.body?:grant_type=client_credentials}") final String oauth2RequestBody,
                             @Value("${api.twitter.trends.place.uri?:https://api.twitter.com/1.1/trends/place.json}") final String trendsByPlaceBaseURI,
                             @Value("${api.twitter.trends.place.parameters?:id={woeId}}") final String[] trendsByPlaceParameters,
                             @Value("${api.twitter.search.tweets.uri?:https://api.twitter.com/1.1/search/tweets.json}") final String tweetsByTrendURI,
                             @Value("${api.twitter.search.tweets.parameters?:q={trend},result_type=popular,count=10}") final String[] tweetsByTrendParameters) {
        this.yahooAPI = yahooAPI;
        this.uriHelper = uriHelper;
        this.oauth2URI = URI.create(oauth2URI);
        this.oauth2ConsumerKey = oauth2ConsumerKey;
        this.oauth2ConsumerSecret = oauth2ConsumerSecret;
        this.oauth2Charset = Charset.forName(oauth2Charset);
        this.oauth2RequestBody = oauth2RequestBody;
        this.trendsByPlaceBaseURI = URI.create(trendsByPlaceBaseURI);
        this.trendsByPlaceParameters = uriHelper.getQueryParametersAsMap(trendsByPlaceParameters);
        this.tweetsByTrendURI = URI.create(tweetsByTrendURI);
        this.tweetsByTrendParameters = uriHelper.getQueryParametersAsMap(tweetsByTrendParameters);
    }

    @Override
    public List<Trend> getTrendsByCountry(final String countryCode) {
        // TODO: could run the following two lines in parallel
        String authToken = getAppAuthToken(oauth2ConsumerKey, oauth2ConsumerSecret);
        String woeId = yahooAPI.getWOEId(countryCode);
        HttpEntity<String> entity = getAuthHeadedEntity(authToken);
        URI getTrendsByWOEIdURI = uriHelper.createURI(trendsByPlaceBaseURI, trendsByPlaceParameters, ImmutableMap.of("woeId", woeId));
        List<InboundTrend> trendResponses = new ArrayList<>();
        RestTemplate restTemplate = new RestTemplate();
        log.debug("Requesting Twitter trends from their API for WOEId: '{}", woeId);
        try {
            ResponseEntity<List<com.newswhip.test.model.inbound.twitter.trend.Container>> trendsResponse = restTemplate.exchange(getTrendsByWOEIdURI, HttpMethod.GET, entity, new ParameterizedTypeReference<List<com.newswhip.test.model.inbound.twitter.trend.Container>>(){});
            if (trendsResponse.getBody() != null) {
                log.info("Received Twitter trends for WOEId: '{}'", woeId);
                trendsResponse.getBody().get(0).getTrends().forEach(trendResponse -> {
                    if (trendResponse.getTweetVolume() > 0) {
                        trendResponses.add(trendResponse);
                    }
                });
            }
        }
        catch (RestClientResponseException exception) {
            log.error("Requesting trends from Twitter for WOEId'{}'; status code: '{}'; message: '{}'",
                    woeId, exception.getRawStatusCode(), exception.getResponseBodyAsString());
            throw exception;
        }
        trendResponses.sort((trendResponse1, trendResponse2) -> {
            if (trendResponse1.getTweetVolume() == trendResponse2.getTweetVolume()) {
                return 0;
            }
            return trendResponse1.getTweetVolume() < trendResponse2.getTweetVolume() ? 1 : -1;
        });
        return trendResponses.stream().map(Trend::new).collect(Collectors.toList());
    }

    @Override
    public List<Tweet> getTop10TweetsByTrend(final Trend trend) {
        String authToken = getAppAuthToken(oauth2ConsumerKey, oauth2ConsumerSecret);
        HttpEntity<String> entity = getAuthHeadedEntity(authToken);
        URI getTweetsByTrendURI = uriHelper.createURI(tweetsByTrendURI, tweetsByTrendParameters, ImmutableMap.of("trend", trend.getName()));
        RestTemplate restTemplate = new RestTemplate();
        List<Tweet> tweets = new ArrayList<>();
        log.debug("Requesting tweets from Twitter for trend: '{}'", trend.getName());
        try {
            ResponseEntity<Container> tweetsResponse = restTemplate.exchange(getTweetsByTrendURI, HttpMethod.GET, entity, Container.class);
            if (tweetsResponse.getBody() != null) {
                log.info("Received tweets from Twitter for trend: '{}'", trend.getName());
                tweets.addAll(tweetsResponse.getBody().getInboundTweets().stream().map(Tweet::new).collect(Collectors.toList()));
            }
        }
        catch (RestClientResponseException exception) {
            log.error("Requesting tweets from Twitter for trend'{}'; status code: '{}'; message: '{}'",
                    trend.getName(), exception.getRawStatusCode(), exception.getResponseBodyAsString());
            throw exception;
        }
        return tweets;
    }

    protected String getAppAuthToken(final String consumerKey, final String consumerSecret) {
        if (oauth2Token != null && !oauth2Token.trim().isEmpty()) {
            log.debug("Retrieved Twitter OAuth2 token from cache");
            return oauth2Token;
        }
        String bearerToken = getBase64EncodedBearerToken(consumerKey, consumerSecret);
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Basic " + bearerToken);
        headers.setContentType(new MediaType(APPLICATION_FORM_URLENCODED, oauth2Charset));
        HttpEntity<String> entity = new HttpEntity<>(oauth2RequestBody, headers);
        RestTemplate restTemplate = new RestTemplate();
        log.debug("Requesting OAuth2 token from Twitter");
        try {
            ResponseEntity<Response> appTokenResponse = restTemplate.exchange(oauth2URI, HttpMethod.POST, entity, Response.class);
            if (appTokenResponse.getBody() != null && appTokenResponse.getBody().getTokenType().equals("bearer")) {
                log.info("Received OAuth2 token from Twitter; will cache for further use");
                oauth2Token = appTokenResponse.getBody().getToken();
            }
        }
        catch (RestClientResponseException exception) {
            log.error("Requesting OAuth2 token from Twitter message: '{}'", exception.getMessage());
            throw exception;
        }
        return oauth2Token;
    }

    private String urlEncode(final String forEncoding) {
        String encoded;
        try {
            encoded = uriHelper.urlEncode(forEncoding);
        } catch (UnsupportedEncodingException e) {
            encoded = forEncoding;
        }
        return encoded;
    }

    protected String getBase64EncodedBearerToken(final String consumerKey, final String consumerSecret) {
        // Doesn't change the Strings but recommended to do this by the Twitter docs for future proofing
        String rfcEncodedKey = urlEncode(consumerKey);
        String rfcEncodedSecret = urlEncode(consumerSecret);
        String bearerToken = rfcEncodedKey + ":" + rfcEncodedSecret;
        return Base64.getEncoder().encodeToString(bearerToken.getBytes());
    }

    private <T> HttpEntity<T> getAuthHeadedEntity(final String authToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + authToken);
        return new HttpEntity<>(headers);
    }

}
