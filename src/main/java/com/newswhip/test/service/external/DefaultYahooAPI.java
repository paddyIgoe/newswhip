package com.newswhip.test.service.external;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.newswhip.test.model.inbound.yahoo.woeId.Container;
import com.newswhip.test.util.URIHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import javax.inject.Inject;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Component
@Slf4j
public class DefaultYahooAPI implements YahooAPI {

    private final URIHelper uriHelper;
    private final ResourceLoader resourceLoader;

    private final URI yqlBaseURI;
    private final Map<String, String> woeIdQueryParameters;
    private final String defaultWOEId;

    private final String woeIdCacheLocation;
    private Map<String, String> countryByWOEIdCache;

    @Inject
    public DefaultYahooAPI(final URIHelper uriHelper,
                           final ResourceLoader resourceLoader,
                           @Value("${api.yahoo.yql.uri?:https://query.yahooapis.com/v1/public/yql}") final String yqlBaseURI,
                           @Value("${api.yahoo.yql.woeId.parameters?:q=select woeid from geo.places where text=\"{countryCode}\" and placetype=\"Country\",format=json}") final String[] woeIdQueryParameters,
                           @Value("${api.yahoo.yql.woeId.default?:1}") final String defaultWOEId,
                           @Value("${api.yahoo.yql.woeId.cache.location?:classpath:/config/countryCode_WOEId.properties}") final String woeIdCacheLocation) {
        this.uriHelper = uriHelper;
        this.resourceLoader = resourceLoader;
        this.yqlBaseURI = URI.create(yqlBaseURI);
        this.woeIdQueryParameters = uriHelper.getQueryParametersAsMap(woeIdQueryParameters);
        this.defaultWOEId = defaultWOEId;
        this.woeIdCacheLocation = woeIdCacheLocation;
        this.countryByWOEIdCache = getWOEIdCache(woeIdCacheLocation);
    }

    @Override
    public String getWOEId(final String countryCode) {
        String woeId = countryByWOEIdCache.get(countryCode);
        if (woeId != null) {
            log.debug("Found WOEId: '{}' for countryCode:'{}' in cache", woeId, countryCode);
            return countryByWOEIdCache.get(countryCode);
        }
        woeId = defaultWOEId;
        RestTemplate restTemplate = new RestTemplate();
        URI getWOEIdURI = uriHelper.createURI(yqlBaseURI, woeIdQueryParameters, ImmutableMap.of("countryCode", countryCode));
        log.debug("Requesting WOEId from Yahoo for countryCode: '{}'", countryCode);
        try {
            ResponseEntity<Container> response = restTemplate.getForEntity(getWOEIdURI, Container.class);
            if (response.getBody() != null) {
                woeId = response.getBody().getQuery().getResult().getFirstPlace().getWoeId();
                log.info("Received WOEId: '{}' from Yahoo for countryCode: '{}'", countryCode);
                countryByWOEIdCache.put(countryCode, woeId);
                new Thread(() -> writeWOEIdsToFile(countryByWOEIdCache, woeIdCacheLocation, resourceLoader)).start();
            }
        }
        catch (RestClientResponseException exception) {
            log.error("Requesting a WOEId from Yahoo for countryCode: '{}'; status code: '{}'; message: '{}'",
                    countryCode, exception.getRawStatusCode(), exception.getResponseBodyAsString());
            throw exception;
        }
        return woeId;
    }

    protected Map<String, String> getWOEIdCache(final String woeIdsFilePath) {
        log.debug("Loading WOEId cache file from resource: '{}'", woeIdsFilePath);
        Resource woeIdSource = resourceLoader.getResource(woeIdsFilePath);
        Properties woeIdProperties = new Properties();
        try (InputStream inputStream = woeIdSource.getInputStream()) {
            woeIdProperties.load(inputStream);
        }
        catch (IOException e) {
            log.error("Unable to read WOEId cache file; message: '{}'", e.getMessage());
            // If cache isn't functional then API will be used. No need to throw an exception as app can recover
        }
        return new ConcurrentHashMap<>(woeIdProperties.entrySet().stream()
                .collect(Collectors.toMap(e -> e.getKey().toString(), e -> e.getValue().toString())));
    }

    // TODO: This method doesn't seem to be writing the cache properly
    protected void writeWOEIdsToFile(final Map<String, String> woeIds, final String woeIdsFileLocation, final ResourceLoader resourceLoader) {
        log.debug("Writing WOEIds cache to resource: '{}'", woeIdsFileLocation);
        Properties woeIdProperties = new Properties();
        woeIdProperties.putAll(woeIds);
        Resource woeIdSource = resourceLoader.getResource(woeIdsFileLocation);
        File woeIdsFile;
        try {
            woeIdsFile = woeIdSource.getFile();
        }
        catch (IOException e) {
            log.error("Unable to retrieve WOEId cache file at: '{}'; message: '{}'", woeIdsFileLocation, e.getMessage());
            return;
        }
        try (FileWriter writer = new FileWriter(woeIdsFile)) {
            woeIdProperties.store(writer, null);
        }
        catch (IOException e) {
            log.error("Unable to write WOEId cache file to '{}'; message: '{}'", woeIdsFile.getPath(), e.getMessage());
        }
        // App still functions without the cache so no need to throw exceptions or do anything drastic
        // in the case of an exception writing to the cache
    }

}

