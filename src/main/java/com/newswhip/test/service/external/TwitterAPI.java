package com.newswhip.test.service.external;

import com.newswhip.test.model.Trend;
import com.newswhip.test.model.Tweet;

import java.util.List;

public interface TwitterAPI {

    List<Trend> getTrendsByCountry(String countryCode);
    List<Tweet> getTop10TweetsByTrend(Trend trend);

}
