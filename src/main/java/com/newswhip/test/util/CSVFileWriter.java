package com.newswhip.test.util;

import java.io.File;
import java.io.IOException;
import java.util.List;

public interface CSVFileWriter {

    void write(File outputFile, List<String> headers,List<List<String>> dataByRow) throws IOException;

}
