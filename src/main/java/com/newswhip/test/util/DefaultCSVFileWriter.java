package com.newswhip.test.util;

import com.opencsv.CSVWriter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class DefaultCSVFileWriter implements CSVFileWriter {

    @Override
    public void write(File outputFile, List<String> headers, List<List<String>> dataByRow) throws IOException {
        try (FileWriter writer = new FileWriter(outputFile);
             CSVWriter csvWriter = new CSVWriter(writer)) {
            List<String[]> data = new ArrayList<>();
            data.add(headers.toArray(new String[0]));
            dataByRow.forEach(row -> data.add(row.toArray(new String[0])));
            csvWriter.writeAll(data);
        }
    }

}
