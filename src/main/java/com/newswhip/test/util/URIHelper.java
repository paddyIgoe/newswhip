package com.newswhip.test.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
public class URIHelper {

    private final Charset characterEncoding;

    public URIHelper(@Value("${charset.default?:UTF-8}") final String characterEncoding) {
        this.characterEncoding = Charset.forName(characterEncoding);
    }

    public URI createURI(final String baseURI, final Map<String, String> queryParameters, final Map<String, String> uriVariables) {
        URI uri = URI.create(baseURI);
        return createURI(uri, queryParameters, uriVariables);
    }

    public URI createURI(final URI baseURI, final Map<String, String> queryParameters, final Map<String, String> uriVariables) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUri(baseURI);
        queryParameters.forEach(builder::queryParam);
        return builder.buildAndExpand(uriVariables).toUri();
    }

    public Map<String, String> getQueryParametersAsMap(final String[] queryParameters) {
        Map<String, String> queryParameterMap = new HashMap<>();
        for(String queryParameter : queryParameters) {
            String[] parameterSplit = queryParameter.split("=", 2);
            if (parameterSplit.length == 2) {
                queryParameterMap.put(parameterSplit[0], parameterSplit[1]);
            }
            else {
                log.error("Request URI parameter '{}' must be in the format: <key>=<value> e.g. format=json", queryParameter);
                throw new IllegalArgumentException("There was a problem retrieving a WOEId from Yahoo; see the log for more details");
            }
        }
        return queryParameterMap;
    }

    public String urlEncode(final String forEncoding) throws UnsupportedEncodingException {
        return urlEncode(forEncoding, characterEncoding);
    }

    public String urlEncode(final String forEncoding, final Charset charset) throws UnsupportedEncodingException {
        return URLEncoder.encode(forEncoding, charset.toString());
    }

}
