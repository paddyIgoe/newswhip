package com.newswhip.test.validation;

import com.google.common.collect.ImmutableSet;
import com.neovisionaries.i18n.CountryCode;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import java.util.Set;

@Component
public class CountryCode2CharValidator extends StringValidator {

    private final Set<String> validCountryCodes;

    public CountryCode2CharValidator(@Value("${validation.countryCodes?:IE,UK,US}") final String[] validCountryCodes) {
        this.validCountryCodes = ImmutableSet.copyOf(validCountryCodes);
    }

    @Override
    public void validate(final Object target, Errors errors) {
        String countryCode = (String) target;
        super.validate(countryCode, errors);
        if (!validCountryCodes.contains(countryCode) || CountryCode.getByAlpha2Code(countryCode) == null) {
            errors.reject("countryCode.invalid", "Should be ISO 3166-1 alpha-2 compliant, i.e. 2 character country identifier, e.g. 'IE'");
        }
    }


}
