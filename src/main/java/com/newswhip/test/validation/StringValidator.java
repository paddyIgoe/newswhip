package com.newswhip.test.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class StringValidator implements Validator {

    @Override
    public boolean supports(final Class<?> clazz) {
        return String.class.equals(clazz);
    }

    @Override
    public void validate(final Object target, Errors errors) {
        String stringForValidation = (String) target;
        if (isNull(stringForValidation)) {
            errors.reject("string.null", "Should not be null");
        }
        if (isEmpty(stringForValidation)) {
            errors.reject("string.empty", "Should not be empty");
        }
        if (isWhiteSpace(stringForValidation)) {
            errors.reject("string.whitespace", "Should not be whitespace");
        }
    }

    protected boolean isNull(final String stringForValidation) {
        return stringForValidation == null;
    }

    protected boolean isEmpty(final String stringForValidation) {
        return stringForValidation.isEmpty();
    }

    protected boolean isWhiteSpace(final String stringForValidation) {
        return stringForValidation.trim().isEmpty();
    }

}
